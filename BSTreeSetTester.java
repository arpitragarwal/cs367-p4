///////////////////////////////////////////////////////////////////////////////
// Title:            (Program 4)
// File:             BSTreeSetTester.java
// Semester:         (CS 367) Spring 2016
//
// Author:           (Tone Yu, Arpit Agarwal)
// Email:            (tyu44@wisc.edu)
// CS Login:         (tone)
// Lecturer's Name:  (Skrenty)
//
////////////////////////////////////////////////////////////////////////////////
//
// Pair Partner:     (Arpit Agarwal)
// Email:            (agarwal32@wisc.edu)
// CS Login:         (arpit)
// Lecturer's Name:  (Skrenty)
////////////////////////////////////////////////////////////////////////////////

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * SetTesterADT implementation using a Binary Search Tree (BST) as the data
 * structure.
 *
 * <p>The BST rebalances if a specified threshold is exceeded (explained below).
 * If rebalanceThreshold is non-positive (&lt;=0) then the BST DOES NOT
 * rebalance. It is a basic BStree. If the rebalanceThreshold is positive
 * then the BST does rebalance. It is a BSTreeB where the last B means the
 * tree is balanced.</p>
 *
 * <p>Rebalancing is triggered if the absolute value of the balancedFfactor in
 * any BSTNode is &gt;= to the rebalanceThreshold in its BSTreeSetTester.
 * Rebalancing requires the BST to be completely rebuilt.</p>
 *
 * @author CS367
 */
public class BSTreeSetTester <K extends Comparable<K>> implements SetTesterADT<K>{

    /** Root of this tree */
    BSTNode<K> root;

    /** Number of items in this tree*/
    int numKeys;

    /**
     * rebalanceThreshold &lt;= 0 DOES NOT REBALANCE (BSTree).<br>
     * rebalanceThreshold  &gt;0 rebalances the tree (BSTreeB).
      */
    int rebalanceThreshold;

    /**
     * True iff tree is balanced, i.e., if rebalanceThreshold is NOT exceeded
     * by absolute value of balanceFactor in any of the tree's BSTnodes.Note if
     * rebalanceThreshold is non-positive, isBalanced must be true.
     */
    boolean isBalanced;


    /**
     * Constructs an empty BSTreeSetTester with a given rebalanceThreshold.
     *
     * @param rbt the rebalance threshold
     */
    public BSTreeSetTester(int rbt) {
        //TODO
    	root = null;
    	numKeys = 0;
    	rebalanceThreshold = rbt;
    	isBalanced = true;
    }

    /**
     * Add node to binary search tree. Remember to update the height and
     * balancedFfactor of each node. Also rebalance as needed based on
     * rebalanceThreshold.
     *
     * @param key the key to add into the BST
     * @throws IllegalArgumentException if the key is null
     * @throws DuplicateKeyException if the key is a duplicate
     */
    public void add(K key) {
        //TODO 
    	if(key==null) throw new IllegalArgumentException();
    	root = add(root, key);
    	numKeys++;
    	if(!isBalanced) rebalance();
    }
    private BSTNode<K> add(BSTNode<K> n, K key){
    	if(n==null) {
    		return new BSTNode<K>(key);
    	}
    	if(key.equals(n.getKey())){
    		throw new DuplicateKeyException();
    	}
    	if(key.compareTo(n.getKey()) < 0){
    		n.setLeftChild(add(n.getLeftChild(), key));
    	} else n.setRightChild(add(n.getRightChild(), key));
    	
    	int ht_left = 0;
    	int ht_right = 0;
    	if(n.getLeftChild() != null) ht_left = n.getLeftChild().getHeight();
    	if(n.getRightChild() != null) ht_right = n.getRightChild().getHeight();
    	if(ht_left >= ht_right) n.setHeight(ht_left+1);
    	else n.setHeight(ht_right+1);
    	n.setBalanceFactor(ht_left-ht_right);
    	if(Math.abs(n.getBalanceFactor())>rebalanceThreshold && rebalanceThreshold>0) isBalanced = false;
    	return n;
    }
    
    /**
     * Rebalances the tree by:
     * 1. Copying all keys in the BST in sorted order into an array.
     *    Hint: Use your BSTIterator.
     * 2. Rebuilding the tree from the sorted array of keys.
     */
    public void rebalance() {
        K[] keys = (K[]) new Comparable[numKeys];
        //TODO
        Iterator<K> itr = iterator();
        int index = 0;
        while(itr.hasNext()){
        	keys[index] = itr.next();
        	index++;
        }
        root = sortedArrayToBST(keys, 0, numKeys-1);
        isBalanced = true;
    }

    /**
     * Recursively rebuilds a binary search tree from a sorted array.
     * Reconstruct the tree in a way similar to how binary search would explore
     * elements in the sorted array. The middle value in the sorted array will
     * become the root, the middles of the two remaining halves become the left
     * and right children of the root, and so on. This can be done recursively.
     * Remember to update the height and balanceFactor of each node.
     *
     * @param keys the sorted array of keys
     * @param start the first index of the part of the array used
     * @param stop the last index of the part of the array used
     * @return root of the new balanced binary search tree
     */
    private BSTNode<K> sortedArrayToBST(K[] keys, int start, int stop) {
        //TODO
    	if(start>stop) return null;
    	int mid = (start + stop)/2;
    	BSTNode<K> mid_node = new BSTNode<K>(keys[mid]);
    	mid_node.setLeftChild(sortedArrayToBST(keys, start, mid-1));
    	mid_node.setRightChild(sortedArrayToBST(keys, mid+1, stop));
    	//Obtaining heights, setting height
    	int ht_left, ht_right;
    	if(mid_node.getLeftChild()==null){
    		ht_left = 0;
    	}else{
    		ht_left = mid_node.getLeftChild().getHeight();
    	}
    	if(mid_node.getRightChild()==null){
    		ht_right = 0;
    	}else{
    		ht_right = mid_node.getRightChild().getHeight();
    	}
    	if(ht_left>=ht_right) mid_node.setHeight(ht_left+1);
    	else mid_node.setHeight(ht_right+1);
    	//Setting balance factor
    	mid_node.setBalanceFactor(ht_left-ht_right);
        return mid_node;
    }

    /**
     * Returns true iff the key is in the binary search tree.
     *
     * @param key the key to search
     * @return true if the key is in the binary search tree
     * @throws IllegalArgumentException if key is null
     */
    public boolean contains(K key) {
        //TODO
    	if(key==null) throw new IllegalArgumentException();
        return contains(root, key);
    }
    private boolean contains(BSTNode<K> root, K key){
    	if(root==null) return false;
    	if(root.getKey().equals(key)) return true;
    	else return (contains(root.getLeftChild(), key) || contains(root.getRightChild(), key));
    }

    /**
     * Returns the sorted list of keys in the tree that are in the specified
     * range (inclusive of minValue, exclusive of maxValue). This can be done
     * recursively.
     *
     * @param minValue the minimum value of the desired range (inclusive)
     * @param maxValue the maximum value of the desired range (exclusive)
     * @return the sorted list of keys in the specified range
     * @throws IllegalArgumentException if either minValue or maxValue is
     * null, or minValue is larger than maxValue
     */
    public List<K> subSet(K minValue, K maxValue) {
        //TODO
    	if(minValue==null||maxValue==null||maxValue.compareTo(minValue)<0) throw new IllegalArgumentException();
        List<K> keys = new ArrayList<K>();
        return subSet(keys, root, minValue, maxValue);
    }
    private List<K> subSet(List<K> keys, BSTNode<K> root, K minValue, K maxValue){
    	if(root==null) return keys;
    	if(root.getKey().compareTo(minValue)>=0 && root.getKey().compareTo(maxValue)<=0){
    		keys = subSet(keys, root.getLeftChild(), minValue, maxValue);
    		keys.add(root.getKey());
    		keys = subSet(keys, root.getRightChild(), minValue, maxValue);
    	}    	
    	return keys;
    }

    /**
     * Return an iterator for the binary search tree.
     * @return the iterator
     */
    public Iterator<K> iterator() {
        //TODO done
        return new BSTIterator<K>(root);
    }

    /**
     * Clears the tree, i.e., removes all the keys in the tree.
     */
    public void clear() {
        //TODO
    	root = null;
    	numKeys = 0;
    	isBalanced = true;    	
    }

    /**
     * Returns the number of keys in the tree.
     *
     * @return the number of keys
     */
    public int size() {
        //TODO done
        return numKeys;
    }

    /**
     * Displays the top maxNumLevels of the tree. DO NOT CHANGE IT!
     *
     * @param maxDisplayLevels from the top of the BST that will be displayed
     */
    public void displayTree(int maxDisplayLevels) {
        if (rebalanceThreshold > 0) {
            System.out.println("---------------------------" +
                    "BSTreeBSet Display-------------------------------");
        } else {
            System.out.println("---------------------------" +
                    "BSTreeSet Display--------------------------------");   
        }
        displayTreeHelper(root, 0, maxDisplayLevels);
    }

    private void displayTreeHelper(BSTNode<K> n, int curDepth,
                                   int maxDisplayLevels) {
        if(maxDisplayLevels <= curDepth) return;
        if (n == null)
            return;
        for (int i = 0; i < curDepth; i++) {
            System.out.print("|--");
        }
        System.out.println(n.getKey() + "[" + n.getHeight() + "]{" +
                n.getBalanceFactor() + "}");
        displayTreeHelper(n.getLeftChild(), curDepth + 1, maxDisplayLevels);
        displayTreeHelper(n.getRightChild(), curDepth + 1, maxDisplayLevels);
    }
}
